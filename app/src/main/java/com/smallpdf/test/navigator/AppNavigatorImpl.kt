package com.smallpdf.test.navigator

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.smallpdf.test.R
import javax.inject.Inject


class AppNavigatorImpl @Inject constructor(private val activity: FragmentActivity) : AppNavigator {

    override fun navigateTo(fragment: Fragment) {

        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, fragment)
            .addToBackStack(fragment::class.java.canonicalName)
            .commit()
    }

}
