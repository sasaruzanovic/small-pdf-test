package com.smallpdf.test.navigator

import androidx.fragment.app.Fragment

interface AppNavigator {
    // Navigate to a given screen.
    fun navigateTo(fragment: Fragment)
}
