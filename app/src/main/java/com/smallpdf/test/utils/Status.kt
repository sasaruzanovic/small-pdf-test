package com.smallpdf.test.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}