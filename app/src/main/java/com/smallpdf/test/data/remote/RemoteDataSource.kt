package com.smallpdf.test.data.remote

import com.smallpdf.test.data.DataSource
import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.data.model.Repository
import com.smallpdf.test.data.model.User
import com.smallpdf.test.data.model.commit.CommitDetails
import com.smallpdf.test.utils.*
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val apiService: GitHubService) :
    DataSource {

    override suspend fun getUserDetails(userId: String): Resource<User> {
        return safeApiCall { apiService.getUserDetails(userId) }
    }

    override suspend fun getUserRepos(userId: String): Resource<List<Repository>> {
        return safeApiCall { apiService.getUserRepos(userId) }
    }

    override suspend fun getBranchesForRepo(
        userId: String,
        repoId: String
    ): Resource<List<Branch>> {
        return safeApiCall { apiService.getBranchesForRepo(userId, repoId) }
    }

    override suspend fun getCommitsForBranch(
        userId: String,
        repoId: String,
        branchId: String
    ): Resource<List<CommitDetails>> {
        return safeApiCall { apiService.getCommitsForBranch(userId, repoId, branchId) }
    }

    override suspend fun getCommitDetails(
        userId: String,
        repoId: String,
        commitId: String
    ): Resource<CommitDetails> {
        return safeApiCall { apiService.getCommitDetails(userId, repoId, commitId) }
    }


    private suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Resource<T> {
        return try {

            return when (val response = ApiResponse.create(call())) {
                is ApiSuccessResponse -> {
                    Resource.success(response.data)
                }
                is ApiErrorResponse -> Resource.error(response.errorMessage, null)
                is ApiSuccessEmptyResponse -> Resource.error("Error empty", null)
            }

        } catch (exception: Exception) {
            // An exception was thrown when calling the API so we're converting this to an IOException
            Resource.error(exception.toString(), null)
        }
    }

}