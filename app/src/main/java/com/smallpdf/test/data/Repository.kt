package com.smallpdf.test.data

import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.data.model.Repository
import com.smallpdf.test.data.model.User
import com.smallpdf.test.data.model.commit.CommitDetails
import com.smallpdf.test.utils.Resource

interface Repository {

    suspend fun getUserDetails(userId: String): Resource<User>

    suspend fun getUserRepos(userId: String): Resource<List<Repository>>

    suspend fun getBranchesForRepo(userId: String, repoId: String): Resource<List<Branch>>

    suspend fun getCommitsForBranch(
        userId: String, repoId: String, branchId: String
    ): Resource<List<CommitDetails>>

    suspend fun getCommitDetails(
        userId: String, repoId: String, commitId: String
    ): Resource<CommitDetails>
}
