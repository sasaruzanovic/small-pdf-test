package com.smallpdf.test.data.model

data class Branch(
    val name: String
)