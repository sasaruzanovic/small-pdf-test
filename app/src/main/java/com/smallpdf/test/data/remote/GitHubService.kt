package com.smallpdf.test.data.remote

import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.data.model.Repository
import com.smallpdf.test.data.model.User
import com.smallpdf.test.data.model.commit.CommitDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubService {

    @GET("users/{userID}")
    suspend fun getUserDetails(@Path("userID") userId: String): Response<User>

    @GET("users/{userID}/repos")
    suspend fun getUserRepos(@Path("userID") userId: String): Response<List<Repository>>

    @GET("repos/{userID}/{repoId}/branches")
    suspend fun getBranchesForRepo(
        @Path("userID") userId: String,
        @Path("repoId") repoId: String
    ): Response<List<Branch>>

    @GET("repos/{userID}/{repoId}/commits")
    suspend fun getCommitsForBranch(
        @Path("userID") userId: String,
        @Path("repoId") repoId: String,
        @Query("sha") branchId: String
    ): Response<List<CommitDetails>>

    @GET("repos/{userID}/{repoId}/commits/{commitId}")
    suspend fun getCommitDetails(
        @Path("userID") userId: String,
        @Path("repoId") repoId: String,
        @Path("commitId") commitId: String
    ): Response<CommitDetails>

}