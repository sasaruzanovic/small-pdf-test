package com.smallpdf.test.data.model.commit

data class Stats(

    val total: Int,
    val additions: Int,
    val deletions: Int
)