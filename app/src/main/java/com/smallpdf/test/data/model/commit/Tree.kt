package com.smallpdf.test.data.model.commit

data class Tree(

    val sha: String,
    val url: String
)