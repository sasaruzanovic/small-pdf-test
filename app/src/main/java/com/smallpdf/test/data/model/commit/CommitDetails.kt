package com.smallpdf.test.data.model.commit

data class CommitDetails(

    val sha: String,
    val node_id: String,
    val commit: Commit,
    val url: String,
    val html_url: String,
    val comments_url: String,
    val author: Author,
    val committer: Committer,
    val parents: List<Parents>,
    val stats: Stats,
    val files: List<File>
)