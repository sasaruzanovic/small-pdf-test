package com.smallpdf.test.data

import javax.inject.Inject


class MainRepository @Inject constructor(private val remoteDataSource: DataSource) :
    Repository {

    override suspend fun getUserDetails(userId: String) = remoteDataSource.getUserDetails(userId)
    override suspend fun getUserRepos(userId: String) = remoteDataSource.getUserRepos(userId)
    override suspend fun getBranchesForRepo(
        userId: String,
        repoId: String
    ) = remoteDataSource.getBranchesForRepo(userId, repoId)

    override suspend fun getCommitsForBranch(
        userId: String,
        repoId: String,
        branchId: String
    ) = remoteDataSource.getCommitsForBranch(userId, repoId, branchId)

    override suspend fun getCommitDetails(
        userId: String,
        repoId: String,
        commitId: String
    ) = remoteDataSource.getCommitDetails(userId, repoId, commitId)

}