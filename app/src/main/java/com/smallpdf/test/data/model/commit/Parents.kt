package com.smallpdf.test.data.model.commit

data class Parents(

    val sha: String,
    val url: String,
    val html_url: String
)