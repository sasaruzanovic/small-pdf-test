package com.smallpdf.test.ui.branch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.smallpdf.test.R
import com.smallpdf.test.ui.BaseFragment
import com.smallpdf.test.ui.ListActions
import com.smallpdf.test.ui.commit.CommitListFragment
import com.smallpdf.test.utils.Status
import kotlinx.android.synthetic.main.fragment_branch_list.*

private const val ARG_USER_ID = "user_id"
private const val ARG_REPO_ID = "repo_id"

class BranchListFragment : BaseFragment(), ListActions {
    private var repositoryId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(ARG_USER_ID)
            repositoryId = it.getString(ARG_REPO_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_branch_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context)
        recycleViewBranches.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            recycleViewBranches.context,
            layoutManager.orientation
        )
        recycleViewBranches.addItemDecoration(dividerItemDecoration)

        model.branches.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {
                    recycleViewBranches.adapter =
                        response.data?.let { BranchAdapter(it, this) }

                }
                else -> handleRequestStatus(response)
            }
        })

        val userId = this.userId
        val repoId = this.repositoryId

        if (userId != null && repoId != null) {
            model.fetchBranchesForRepo(userId, repoId)
        }

    }


    companion object {
        @JvmStatic
        fun newInstance(userID: String, repoID: String) =
            BranchListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER_ID, userID)
                    putString(ARG_REPO_ID, repoID)
                }
            }
    }

    override fun openDetails(id: String) {
        val userId = this.userId
        val repoId = this.repositoryId

        if (userId != null && repoId != null) {
            navigator.navigateTo(CommitListFragment.newInstance(userId, repoId, id))
        }
    }
}