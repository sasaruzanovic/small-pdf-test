package com.smallpdf.test.ui

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.smallpdf.test.R
import com.smallpdf.test.navigator.AppNavigator
import com.smallpdf.test.utils.Resource
import com.smallpdf.test.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
open class BaseFragment : Fragment() {

    @Inject
    lateinit var navigator: AppNavigator

    protected val model: MainViewModel by activityViewModels()
    protected var userId: String? = null

    protected fun handleRequestStatus(result: Resource<Any>) {
        if (result.status == Status.ERROR) {
            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
        } else if (result.status == Status.LOADING) {
            Toast.makeText(context, getString(R.string.loading), Toast.LENGTH_SHORT).show()
        }
    }
}