package com.smallpdf.test.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.smallpdf.test.utils.Status

import com.smallpdf.test.R
import com.smallpdf.test.ui.BaseFragment
import com.smallpdf.test.ui.repository.RepositoryListFragment
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.fragment_user.imageViewProfile

private const val ARG_USER_ID = "user_id"

class UserFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(ARG_USER_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.user.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {

                    response.data?.let {
                        textViewName.text = it.name
                        textViewCompany.text = it.company

                        Glide
                            .with(this)
                            .load(it.avatar_url)
                            .centerCrop()
                            .into(imageViewProfile)
                    }
                }
                else -> handleRequestStatus(response)

            }
        })
        userId?.let { model.fetchUser(it) }

        buttonShowRepos.setOnClickListener {
            userId?.let {
                navigator.navigateTo(RepositoryListFragment.newInstance(it))
            }
        }
    }

    companion object {

        fun newInstance(userId: String) =
            UserFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER_ID, userId)
                }
            }
    }
}