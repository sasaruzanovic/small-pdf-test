package com.smallpdf.test.ui.commit

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.smallpdf.test.R
import com.smallpdf.test.ui.BaseFragment
import com.smallpdf.test.utils.Status
import kotlinx.android.synthetic.main.fragment_commit_details.*

private const val ARG_USER_ID = "user_id"
private const val ARG_REPO_ID = "repo_id"
private const val ARG_COMMIT_SHA = "commit_sha"


class CommitDetailsFragment : BaseFragment() {
    private var repoId: String? = null
    private var commitSHA: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(ARG_USER_ID)
            repoId = it.getString(ARG_REPO_ID)
            commitSHA = it.getString(ARG_COMMIT_SHA)
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.commit.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {

                    response.data?.let {
                        textViewCommitterName.text =
                            "${getString(R.string.commiter)}${it.commit.committer.name}"

                        textViewMessage.text =
                            "${getString(R.string.commit_message)}${it.commit.message}"

                        Glide
                            .with(this)
                            .load(it.committer.avatar_url)
                            .centerCrop()
                            .into(imageViewProfile)
                    }

                }
                else -> handleRequestStatus(response)

            }
        })

        val userId = this.userId
        val repoId = this.repoId
        val commitSHA = this.commitSHA

        if (userId != null && repoId != null && commitSHA != null) {
            model.fetchCommit(userId, repoId, commitSHA)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_commit_details, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(userId: String, repoId: String, commitSHA: String) =
            CommitDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER_ID, userId)
                    putString(ARG_REPO_ID, repoId)
                    putString(ARG_COMMIT_SHA, commitSHA)
                }
            }
    }
}