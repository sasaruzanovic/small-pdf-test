package com.smallpdf.test.ui.branch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smallpdf.test.R
import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.ui.ListActions
import kotlinx.android.synthetic.main.item_branch.view.*

class BranchAdapter(
    private val items: List<Branch>,
    private val actions: ListActions
) :
    RecyclerView.Adapter<BranchAdapter.RepoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_branch, parent, false)

        view.setOnClickListener {
            (it.tag as? String)?.let { id -> actions.openDetails(id) }
        }
        return RepoHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RepoHolder, position: Int) {
        val branch = items[position]
        holder.itemView.tag = branch.name
        holder.textViewRepoTile.text = branch.name
    }


    class RepoHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewRepoTile: TextView = view.textViewRepoTitle
    }

}