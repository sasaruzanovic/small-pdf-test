package com.smallpdf.test.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.smallpdf.test.utils.Resource
import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.data.model.User
import com.smallpdf.test.data.model.commit.CommitDetails
import kotlinx.coroutines.launch


class MainViewModel @ViewModelInject constructor(
    private val mainRepository: com.smallpdf.test.data.Repository
) : ViewModel() {

    private val _user = MutableLiveData<Resource<User>>()
    val user: LiveData<Resource<User>>
        get() = _user

    private val _repos = MutableLiveData<Resource<List<com.smallpdf.test.data.model.Repository>>>()
    val repos: LiveData<Resource<List<com.smallpdf.test.data.model.Repository>>>
        get() = _repos

    private val _branches = MutableLiveData<Resource<List<Branch>>>()
    val branches: LiveData<Resource<List<Branch>>>
        get() = _branches

    private val _commits = MutableLiveData<Resource<List<CommitDetails>>>()
    val commits: LiveData<Resource<List<CommitDetails>>>
        get() = _commits

    private val _commit = MutableLiveData<Resource<CommitDetails>>()
    val commit: LiveData<Resource<CommitDetails>>
        get() = _commit

    fun fetchUser(userId: String) {
        _user.postValue(Resource.loading(null))

        viewModelScope.launch {
            mainRepository.getUserDetails(userId).let {
                _user.postValue(it)
            }
        }
    }

    fun fetchUserRepos(userId: String) {
        viewModelScope.launch {
            _repos.postValue(Resource.loading(null))
            mainRepository.getUserRepos(userId).let {
                _repos.postValue(it)
            }
        }
    }

    fun fetchBranchesForRepo(userId: String, repoId: String) {
        viewModelScope.launch {
            _branches.postValue(Resource.loading(null))
            mainRepository.getBranchesForRepo(userId, repoId).let {
                _branches.postValue(it)
            }
        }
    }

    fun fetchCommitsForBranch(userId: String, repoId: String, branchId: String) {
        viewModelScope.launch {
            _commits.postValue(Resource.loading(null))
            mainRepository.getCommitsForBranch(userId, repoId, branchId).let {
                _commits.postValue(it)
            }
        }
    }

    fun fetchCommit(userId: String, repoId: String, commitId: String) {
        viewModelScope.launch {
            _commit.postValue(Resource.loading(null))
            mainRepository.getCommitDetails(userId, repoId, commitId).let {
                _commit.postValue(it)
            }
        }
    }
}