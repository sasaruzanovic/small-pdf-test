package com.smallpdf.test.ui

interface ListActions {
    fun openDetails(id: String)
}