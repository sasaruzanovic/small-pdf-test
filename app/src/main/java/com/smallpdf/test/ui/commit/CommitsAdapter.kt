package com.smallpdf.test.ui.commit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smallpdf.test.R
import com.smallpdf.test.data.model.commit.CommitDetails
import com.smallpdf.test.ui.ListActions
import kotlinx.android.synthetic.main.item_commit.view.*

class CommitsAdapter(
    private val items: List<CommitDetails>,
    private val actions: ListActions
) :
    RecyclerView.Adapter<CommitsAdapter.RepoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_commit, parent, false)

        view.setOnClickListener {
            (it.tag as? String)?.let { id -> actions.openDetails(id) }
        }
        return RepoHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RepoHolder, position: Int) {
        val commitDetails = items[position]
        holder.itemView.tag = commitDetails.sha
        holder.textViewCommitTitle.text = commitDetails.commit.message
    }


    class RepoHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewCommitTitle: TextView = view.textViewCommitTitle
    }

}