package com.smallpdf.test.ui.repository

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smallpdf.test.R
import com.smallpdf.test.data.model.Repository
import com.smallpdf.test.ui.ListActions
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryAdapter(
    private val items: List<Repository>,
    private val actions: ListActions
) :
    RecyclerView.Adapter<RepositoryAdapter.RepoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false)

        view.setOnClickListener {
            (it.tag as? String)?.let { id -> actions.openDetails(id) }
        }
        return RepoHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RepoHolder, position: Int) {
        val repository = items[position]
        holder.itemView.tag = repository.name
        holder.textViewRepoTile.text = repository.name
        holder.textViewOpenIssues.text = repository.open_issues_count.toString()
    }


    class RepoHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewRepoTile: TextView = view.textViewRepoTitle
        val textViewOpenIssues: TextView = view.textViewRepoOpenIssues
    }

}