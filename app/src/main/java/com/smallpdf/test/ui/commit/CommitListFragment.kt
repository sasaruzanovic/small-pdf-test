package com.smallpdf.test.ui.commit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.smallpdf.test.R
import com.smallpdf.test.ui.BaseFragment
import com.smallpdf.test.ui.ListActions
import com.smallpdf.test.utils.Status
import kotlinx.android.synthetic.main.fragment_commit_list.*

private const val ARG_USER_ID = "user_id"
private const val ARG_REPO_ID = "repo_id"
private const val ARG_BRANCH_ID = "branch_id"

class CommitListFragment : BaseFragment(), ListActions {
    private var repoId: String? = null
    private var branchId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(ARG_USER_ID)
            repoId = it.getString(ARG_REPO_ID)
            branchId = it.getString(ARG_BRANCH_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_commit_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context)
        recycleViewCommits.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            recycleViewCommits.context,
            layoutManager.orientation
        )
        recycleViewCommits.addItemDecoration(dividerItemDecoration)

        model.commits.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {

                    recycleViewCommits.adapter =
                        response.data?.let { CommitsAdapter(it, this) }
                }
                else -> handleRequestStatus(response)
            }
        })

        val userId = this.userId
        val repoId = this.repoId
        val branchId = this.branchId

        if (userId != null && repoId != null && branchId != null) {
            model.fetchCommitsForBranch(userId, repoId, branchId)
        }

    }

    companion object {

        @JvmStatic
        fun newInstance(userId: String, repoId: String, branchId: String) =
            CommitListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER_ID, userId)
                    putString(ARG_REPO_ID, repoId)
                    putString(ARG_BRANCH_ID, branchId)
                }
            }
    }

    override fun openDetails(id: String) {

        val userId = this.userId
        val repoId = this.repoId

        if (userId != null && repoId != null) {
            navigator.navigateTo(CommitDetailsFragment.newInstance(userId, repoId, id))
        }
    }
}