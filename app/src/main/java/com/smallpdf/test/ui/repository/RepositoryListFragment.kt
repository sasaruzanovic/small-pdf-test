package com.smallpdf.test.ui.repository

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.smallpdf.test.R
import com.smallpdf.test.ui.BaseFragment
import com.smallpdf.test.ui.ListActions
import com.smallpdf.test.ui.branch.BranchListFragment
import com.smallpdf.test.utils.Status
import kotlinx.android.synthetic.main.fragment_repository_list.*


private const val ARG_USER_ID = "user_id"

class RepositoryListFragment : BaseFragment(), ListActions {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(ARG_USER_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repository_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context)
        recycleViewRepositories.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            recycleViewRepositories.context,
            layoutManager.orientation
        )
        recycleViewRepositories.addItemDecoration(dividerItemDecoration)

        model.repos.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {

                    recycleViewRepositories.adapter =
                        response.data?.let { RepositoryAdapter(it, this) }

                }

                else -> handleRequestStatus(response)
            }
        })
        userId?.let { model.fetchUserRepos(it) }

    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String) =
            RepositoryListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER_ID, param1)
                }
            }
    }

    override fun openDetails(id: String) {
        userId?.let { BranchListFragment.newInstance(it, id) }?.let { navigator.navigateTo(it) }
    }
}