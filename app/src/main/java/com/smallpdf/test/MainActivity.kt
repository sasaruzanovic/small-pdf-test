package com.smallpdf.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.smallpdf.test.navigator.AppNavigator
import com.smallpdf.test.ui.user.UserFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var navigator: AppNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigator.navigateTo(UserFragment.newInstance("octocat"))

    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        }
    }

}