package com.smallpdf.test

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RepoApplication : Application()