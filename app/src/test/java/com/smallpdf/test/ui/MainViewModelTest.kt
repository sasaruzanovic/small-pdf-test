package com.smallpdf.test.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.smallpdf.test.MainCoroutineRule
import com.smallpdf.test.data.FakeRemoteDataSource
import com.smallpdf.test.data.MainRepository
import com.smallpdf.test.getOrAwaitValue
import com.smallpdf.test.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi

class MainViewModelTest {

    private lateinit var mainViewModel: MainViewModel

    private lateinit var mainRepository: MainRepository

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setupViewModel() {
        mainRepository = MainRepository(FakeRemoteDataSource())

        mainViewModel = MainViewModel(mainRepository)
    }

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadUser_loading() {
        mainCoroutineRule.pauseDispatcher()

        mainViewModel.fetchUser("")

        assertTrue(mainViewModel.user.getOrAwaitValue() == Resource.loading(null))

        mainCoroutineRule.resumeDispatcher()

    }
}