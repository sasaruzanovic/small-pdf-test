package com.smallpdf.test.data

import com.smallpdf.test.MainCoroutineRule
import com.smallpdf.test.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MainRepositoryTest {

    private lateinit var remoteDataSource: FakeRemoteDataSource

    // Class under test
    private lateinit var gitHubRepo: MainRepository

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()


    @ExperimentalCoroutinesApi
    @Before
    fun createRepository() {
        remoteDataSource = FakeRemoteDataSource()
        // Get a reference to the class under test
        gitHubRepo = MainRepository(remoteDataSource)
    }


    @ExperimentalCoroutinesApi
    @Test
    fun getUserDetails_userNotExist() = mainCoroutineRule.runBlockingTest {

        val result = gitHubRepo.getUserDetails("test")

        assertTrue(result.status == Status.ERROR)
        assert(result.message == "No user found")

    }

    @ExperimentalCoroutinesApi
    @Test
    fun getUserDetails_userExists() = mainCoroutineRule.runBlockingTest {

        val result = gitHubRepo.getUserDetails("octocat")
        assertTrue(result.status == Status.SUCCESS)
    }
}