package com.smallpdf.test.data

import com.smallpdf.test.data.model.Branch
import com.smallpdf.test.data.model.Repository
import com.smallpdf.test.data.model.User
import com.smallpdf.test.data.model.commit.CommitDetails
import com.smallpdf.test.utils.Resource
import com.smallpdf.test.utils.Status

class FakeRemoteDataSource : DataSource {

    var usersList = ArrayList<User>()
    var reposList = ArrayList<Repository>()
    var branchesList = ArrayList<Branch>()
    var commitsList = ArrayList<CommitDetails>()

    init {
        val user = User(
            "octocat",
            583231,
            "DQ6VXNlcjU4MzIzMQ==M",
            "https://avatars3.githubusercontent.com/u/583231?v=4",
            "",
            "https://api.github.com/users/octocat",
            "https://github.com/octocat",
            "https://api.github.com/users/octocat/followers",
            "https://api.github.com/users/octocat/following{/other_user}",
            "https://api.github.com/users/octocat/gists{/gist_id}",
            "https://api.github.com/users/octocat/starred{/owner}{/repo}",
            "https://api.github.com/users/octocat/subscriptions",
            "https://api.github.com/users/octocat/orgs",
            "https://api.github.com/users/octocat/repos",
            "https://api.github.com/users/octocat/events{/privacy}",
            "https://api.github.com/users/octocat/received_events",
            "User",
            false,
            "The Octocat",
            "@github",
            "https://github.blog",
            "San Francisco",
            "null",
            "null",
            "null",
            "null",
            8,
            8,
            3164,
            9,
            "2011-01-25T18:44:36Z",
            "2020-07-22T14:28:20Z"
        )
        usersList.add(user)
    }

    override suspend fun getUserDetails(userId: String): Resource<User> {
        usersList.find { it.login == userId }?.let {
            return Resource(Status.SUCCESS, it, null)
        }
        return Resource(Status.ERROR, null, "No user found")
    }

    override suspend fun getUserRepos(userId: String): Resource<List<Repository>> {
        return Resource(Status.SUCCESS, reposList, "")
    }

    override suspend fun getBranchesForRepo(
        userId: String,
        repoId: String
    ): Resource<List<Branch>> {
        return Resource(Status.SUCCESS, branchesList, "")
    }

    override suspend fun getCommitsForBranch(
        userId: String,
        repoId: String,
        branchId: String
    ): Resource<List<CommitDetails>> {
        return Resource(Status.SUCCESS, commitsList, "")
    }

    override suspend fun getCommitDetails(
        userId: String,
        repoId: String,
        commitId: String
    ): Resource<CommitDetails> {
        return Resource(Status.SUCCESS, null, "")
    }
}