# README #

This readme file explains in short what I used to solve the problem and also some improvements that
come to my mind but not implemented as the test is time-boxed. Also, it's separated into two topics:
- architecture
- UX

Architecture

Project is based on MVVM architecture as it's officially recommended by Google and well supported.
Also there is repository that provides data to view models. In this case, repository contains only
remote data source but it can be easily extended to support local data source for caching. Therefore
repository and remote data sources are interfaces so they can be changed (or mocked). For remote data
source implementation retrofit with coroutines is used. From UI part there is one activity and few
fragments. There is one view model used in more fragments. From my point of view there is no need to
make view models per fragment as logic is really simple. For dependency injection I used HILT, I know
it's still in alpha but for sure it will become stable (and recommended) in the future and also want to
learn something new with this project.

UX

User data screen has one image, few texts and one button to open repo list. Repo list is just list
that shows all repos along with open issues. And then we come to third screen which I think is not
defined well. It says commit details for each repo, but it's not quite true because commits are related
to branch. So that's why a add one extra step and user pick one branch from the repo and then see
all commits for that branch and finally he can see commit details.